<nav id="sidebar" aria-label="Main Navigation">
    <!-- Side Header -->
    <div class="bg-header-dark">
        <div class="content-header bg-white-10">
            <!-- Logo -->
            <a class="link-fx font-w600 font-size-lg text-white" href="{{route('home')}}">
                <span class="text-white-75">LLC</span><span class="text-white">HRM</span>
            </a>
            <!-- END Logo -->

            <!-- Options -->
            <div>
                <!-- Toggle Sidebar Style -->
                <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                <!-- Class Toggle, functionality initialized in Helpers.coreToggleClass() -->
                <a class="js-class-toggle text-white-75" data-target="#sidebar-style-toggler"
                   data-class="fa-toggle-off fa-toggle-on" data-toggle="layout" data-action="sidebar_style_toggle"
                   href="javascript:void(0)">
                    <i class="fa fa-toggle-off" id="sidebar-style-toggler"></i>
                </a>
                <!-- END Toggle Sidebar Style -->

                <!-- Close Sidebar, Visible only on mobile screens -->
                <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                <a class="d-lg-none text-white ml-2" data-toggle="layout" data-action="sidebar_close"
                   href="javascript:void(0)">
                    <i class="fa fa-times-circle"></i>
                </a>
                <!-- END Close Sidebar -->
            </div>
            <!-- END Options -->
        </div>
    </div>
    <!-- END Side Header -->

    <!-- Side Navigation -->
    <div class="content-side content-side-full">
        <ul class="nav-main">
            @if(auth()->user()->id==1)
                <li class="nav-main-item">
                    <a class="nav-main-link active" href="{{route('admin.dashboard')}}">
                        <i class="nav-main-link-icon si si-cursor"></i>
                        <span class="nav-main-link-name">Dashboard</span>

                    </a>
                </li>
                <li class="nav-main-heading">Base</li>
            @endif


            <li class="nav-main-item">
                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true"
                   aria-expanded="false" href="#">
                    <i class="nav-main-link-icon si si-grid"></i>
                    <span class="nav-main-link-name">Employee Management</span>
                </a>

                <ul class="nav-main-submenu">
                    <li class="nav-main-item">
                        <a class="nav-main-link" href="{{route('employee.list')}}">
                            <span class="nav-main-link-name">Employee Informations</span>
                        </a>
                    </li>
                    @if(auth()->user()->user_type=='admin')

                        <li class="nav-main-item">
                            <a class="nav-main-link" href="{{route('department')}}">
                                <span class="nav-main-link-name">Department</span>
                            </a>
                        </li>
                        <li class="nav-main-item">
                            <a class="nav-main-link" href="{{route('designation')}}">
                                <span class="nav-main-link-name">Designation</span>
                            </a>
                        </li>
                    @endif
                </ul>
            </li>

            <li class="nav-main-item">
                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true"
                   aria-expanded="false" href="#">
                    <i class="nav-main-link-icon si si-grid"></i>
                    <span class="nav-main-link-name">Leave Management</span>
                </a>
                <ul class="nav-main-submenu">
                    @if(auth()->user()->user_type='employee')
                        <li class="nav-main-item">
                            <a class="nav-main-link" href="{{route('requests')}}">
                                <span class="nav-main-link-name">Leave Requests</span>
                            </a>
                        </li>
                    @endif
                    @if(auth()->user()->user_type='admin')
                        <li class="nav-main-item">
                            <a class="nav-main-link" href="{{route('leave_approval')}}">
                                <span class="nav-main-link-name">Leave Approvals</span>
                            </a>
                        </li>

                    @endif
                </ul>


            </li>

            <li class="nav-main-item">
                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true"
                   aria-expanded="false" href="#">
                    <i class="nav-main-link-icon si si-grid"></i>
                    <span class="nav-main-link-name">Attendance Management</span>
                </a>
                <ul class="nav-main-submenu">

                    @if(auth()->user()->user_type='employee')
                        <li class="nav-main-item">
                            <a class="nav-main-link" href="{{route('employee.attendance')}}">
                                <span class="nav-main-link-name">Attendance</span>
                            </a>
                        </li>
                    @endif
                    <li class="nav-main-item">
                        <a class="nav-main-link" href="{{route('attendance.report')}}">
                            <span class="nav-main-link-name">Attendance Report</span>
                        </a>
                    </li>

                </ul>
            </li>

            <li class="nav-main-item">
                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true"
                   aria-expanded="false" href="#">
                    <i class="nav-main-link-icon si si-grid"></i>
                    <span class="nav-main-link-name">Notice</span>
                </a>
                <ul class="nav-main-submenu">
                    <li class="nav-main-item">
                        <a class="nav-main-link" href="{{route('setup_holidays')}}">
                            <span class="nav-main-link-name">Holiday Notice</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link" href="{{route('view_notices')}}">
                            <span class="nav-main-link-name">Other Notices</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-main-item">
                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true"
                   aria-expanded="false" href="#">
                    <i class="nav-main-link-icon si si-grid"></i>
                    <span class="nav-main-link-name">Tasks</span>
                </a>
                <ul class="nav-main-submenu">
                    @if(auth()->user()->user_type='admin')
                        <li class="nav-main-item">
                            <a class="nav-main-link" href="{{route('client.list')}}">
                                <span class="nav-main-link-name">Client Setup</span>
                            </a>
                        </li>
                    @endif
                    <li class="nav-main-item">
                        <a class="nav-main-link" href="{{route('project.list')}}">
                            <span class="nav-main-link-name">Projects</span>
                        </a>
                    </li>
                    @if(auth()->user()->user_type='employee')
                        <li class="nav-main-item">
                            <a class="nav-main-link" href="{{route('workreport.list')}}">
                                <span class="nav-main-link-name"> Work Report </span>
                            </a>
                        </li>
                    @endif

                </ul>
            </li>

            <li>


        </ul>
    </div>
    <!-- END Side Navigation -->
</nav>
