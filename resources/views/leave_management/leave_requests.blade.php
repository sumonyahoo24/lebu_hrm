 @extends('master')
 @section('navbar')

    @include('partials.navbar')
@stop

@section('content')

<div class="container-fluid" style="padding-top: 8px;">


@if(Session::has('message'))
<p class="alert alert-info">{{ Session::get('message') }}</p>
@endif
     
     <button type="button" class="btn btn-success" data-toggle="modal" data-target="#newRequest">
  New Request
     </button>
    
 
      <table class="table table-bordered table-condensed" style="margin-top: 5px;">

            <thead>
            <tr>
               
                <th>From_date</th>
                <th>To_date</th>
                <th>Days</th>
                <th>Leave Reason</th>
                <th>Emergency Contact</th>
                <th>Status</th>
                
            </tr>
            </thead>

            <tbody>
              @foreach($leave as $data)
                <tr>
                    
                    <td>{{$data->from_date}}</td>
                    <td>{{$data->to_date}}</td>
                    <td>{{(floor((strtotime($data->to_date)-strtotime($data->from_date))/3600/24))}}</td>
                    <td>{{$data->leave_reason}}</td>
                    <td>{{$data->emergency_contact}}</td>
                    <td>{{$data->status}}</td>
                </tr>
              @endforeach
            </tbody>
        </table>


<!-- modal -->
<div class="modal fade" id="newRequest" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New Request</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{route('request.create')}}" method="POST" role="form">
            @csrf()
      
          
          

          <div class="form-group">
          <label for="select">Select From Date</label>
           <input type="date" id="from_date" class="form-control" name="from_date">
          </div>

          <div class="form-group">
           <label for="select">Select To Date</label>
           <input type="date" id="to_date" class="form-control" name="to_date">
          </div>


          <div class="form-group">
            <label for="exampleInputReason"> Reason </label>
            <input name="leave_reason" type="text" class="form-control" id="exampleInputreason" aria-describedby="reason" placeholder="Enter Reason">
          </div>


          <div class="form-group">
            <label for="exampleInputNumber">Emergency Contact</label>
            <input name="emergency_contact" type="number" class="form-control" id="exampleInputNumber" placeholder="Enter contact number" required="required">
          </div>

          </div>
          <button  type="submit request" class="btn btn-primary form-control">Submit Request</button>
        </form>
      </div>



</div>
</div>
</div>
</div>



</div>

@stop
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
<!-- <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script> -->
<script type="text/javascript">

</script>