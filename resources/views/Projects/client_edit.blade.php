@extends('master')


@section('navbar')

    @include('partials.navbar')
@stop


@section('content')

    <div class="row">
        @if(Session::has('message'))
            <p class="alert alert-success">{{ Session::get('message') }}</p>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif



        <div class="hero-static offset-md-2 col-md-6 d-flex align-items-bg-white">
            <div class="p-3 w-100">
                <!-- Header -->
                <div class="mb-3 text-center">
                    <p class="text-uppercase font-w700 font-size-sm text-muted">Update Project Information</p>
                </div>

                <form action="{{route('client.updateprocess',$client_info->id)}}" method="POST" role="form"
                      enctype="multipart/form-data">

                    @method('put')
                    @csrf()

                    <div class="form-group">
                        <label for="exampleInputName">Client  Name</label>
                        <input name="client_name" type="name" class="form-control" id="exampleInputName"
                               placeholder="edit Name" value="{{$client_info->client_name}}">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputName">Address</label>
                        <input name="client_address" type="name" class="form-control" id="exampleInputName"
                               placeholder="edit Description" value="{{$client_info->address}}">
                    </div>


                    <div class="form-group">
                        <label for="exampleInputName">Organization</label>
                        <input name="organization" type="name" class="form-control" id="exampleInputName"
                               placeholder="edit Technical Specification" value="{{$client_info->organization}}">
                    </div>


                    <div class="form-group">
                        <label for="exampleInputName">Phone</label>
                        <input name="phone" type="name" class="form-control" id="exampleInputName"
                               placeholder="edit Technical Specification" value="{{$client_info->phone_num}}">
                    </div>




                    <button type="submit" class="btn btn-primary form-control">Submit</button>
                </form>
            </div>

        </div>
    </div>


@stop