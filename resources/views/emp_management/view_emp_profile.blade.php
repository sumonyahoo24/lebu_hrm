@extends('master')
@section('navbar')

    @include('partials.navbar')
@stop
@section('content')
<div class="modal-content">
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="header">
              <h4 class="title" style="margin-left: 20px;">Profile</h4>
            </div>
            <div class="row">
              <div class="col-md-3">
                  <img class="avatar border-white" style="height:200px; width:200px;" img src="{{url('/uploads/images/'.$emp_info->photo)}}">
              </div>
              <div class="col-md-9" style="margin-top: 80px;">
                <div class="col-md-4">
                  <div class="form-group">
                    <span>{{$emp_info->employee_name}}</span>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <span>{{ $emp_info->email }}</span>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <span>{{ $emp_info->phone_num }}</span>
                  </div>
                </div>
              </div>
            </div>
          </div>

        <div class="col-md-12">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group dept" style="padding-top: 25px;">
              <label for="dept"> Department :</label>
             <span>{{$emp_info->department->dept_name}}</span>
              </div>
            </div>
          </div>
            <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="dept">Designation :</label>
                  <span>{{$emp_info->designation->designation_name}}</span>
              </div>
            </div>
          </div>
          </div>
        

        <div class="col-md-12">
          <div class="form-group">
            <label>Address</label>
          </div>
        </div>
</div>
            <div class="col-md-12">
              <div class="col-md-6">
                <div class="form-group">
                  <label>City: </label>
                  <span>Dhaka</span>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Country: </label>
                  <span> Bangladesh</span>
                </div>
              </div>
            </div> 
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
































