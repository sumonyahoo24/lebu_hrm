@extends('master')


@section('navbar')
@include('partials.navbar')
@stop


@section('content')


<div class="container-fluid" style="padding-top: 5px;">



    @if(Session::has('message'))
        <p class="alert alert-success">{{ Session::get('message') }}</p>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


  @if(auth()->user()->id==1)
  <button type="button" id="addEmp" class="btn btn-success" data-toggle="modal" data-target="#addEmployee">
    Add Employee
  </button>
  @endif
  <table class="table table-bordered table-condensed" style="margin-top: 5px;">

    <thead>
      <tr>
        <th>Employee ID</th>
        <th>Emloyee Name</th>
        <th>Photo</th>
        <th>Email</th>
        <th>Department Name</th>
        <th>Designation Name</th>
        <th>Phone Number</th>
        <th>Action</th>
      </tr>
    </thead>

    <tbody>
      @foreach($employee as $data)
      <tr>
        <td>{{$data->id}}</td>
        <td>{{$data->employee_name}}</td>
        <td><img src="{{url('/uploads/images/'.$data->photo)}}" style="height: 50px;width: 50px;"></td>
        <td>{{$data->email}}</td>
        <td>{{$data->dept_name}}</td>
        <td>{{$data->designation_name}}</td>
        <td>{{$data->phone_num}}</td>
        
        <td>
         @if(auth()->user()->id!=1)
         <a class="btn btn-success" href="{{route('update_employee_page',$data->id)}}"><i class="fa fa-edit"></i></a>
         @endif


         <a class="btn btn-info" href="{{route('view_profile',$data->user_id)}}"><i class="fa fa-eye"></i></a>
       </td>
       @endforeach

     </tr>
     
   </tbody>
 </table>


</div>


<!-- Modal -->
<div class="modal fade" id="addEmployee" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add New Employee</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form action="{{route('employee.create')}}" method="POST" enctype="multipart/form-data" role="form">
        @csrf()
        <div class="form-group">
          <label for="exampleInputName">Employee Name</label>
          <input name="employee_name" type="name" class="form-control" id="exampleInputName" placeholder="Enter Name" required="required">
        </div>
        

        <div class="form-group">
          <label for="exampleInputEmail1">Email </label>
          <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" required="required">
        </div>


        <div class="form-group">
          <label for="exampleInputNumber">Phone Number</label>
          <input name="phone_num" type="number" class="form-control" id="exampleInputNumber" placeholder="Enter phone number" required="required">
        </div>

        <div class="form-group">
          <label for="exampleInputPhoto">Photo</label>
          <input name="photo" type="file" class="form-control" id="exampleInputPhoto" placeholder="Upload Photo" required="required">
        </div>
       <div class="form-group">
         <label for="dept">Select Department</label>
         <select name="department_id" class="select form-control" required="required">
          @foreach($departments as $dept)
          <option value="{{$dept->id}}">{{$dept->dept_name}}</option>
          @endforeach
        </select>
        
      </div>
      

      <div class="form-group">
       <label for="desig">Select Designation</label>
       <select name="designation_id" class="select form-control" required="required">
        @foreach($designations as $desig)
        <option value="{{$desig->id}}">{{$desig->designation_name}}</option>
        @endforeach
      </select>
      
    </div>
    <button  type="submit" class="btn btn-primary form-control">Submit</button>
  </form>
</div>

</div>
</div>
</div>
<!-- modal end -->

<script>
  $(document).ready(function(argument) {
    $(document).on('click','#addEmp',function(argument) {
      alert();
    })
  })
</script>

@stop





