@extends('master')


@section('navbar')
    @include('partials.navbar')
@stop


@section('content')


    <!-- Page Content -->
    <div class="bg-image" style="background-image: url('assets/media/photos/photo22@2x.jpg');">



        <div class="row no-gutters bg-primary-op">
            <!-- Main Section -->
            
            <!-- END Main Section -->

            <!-- Meta Info Section -->
            <div class="col-md-12" style="background-color: #16467b;">
                   <marquee>
                    <p style="color: white;">Notice:
                        @foreach($notices as $notice)
                        <span>{{$notice->notice_description}}</span>
                        @endforeach
                        ,  Holiday:
                        @foreach($holidays as $hd)
                        <span>{{$hd->holiday_name}} From : {{$hd->from_date}} To: {{$hd->to_date}}</span>
                        @endforeach
                    </p>
                </marquee>
                </div>
            <div class="hero-static col-md-12 d-none d-md-flex align-items-md-center justify-content-md-center text-md-center">
                
                <div class="p-3">

                    <p class="display-4 font-w700 text-white mb-3">
                       WELCOME TO LETS LEARN CODING
                    </p>
                    
                </div>
            </div>
            
          
    </div>
    <!-- END Page Content-->

@stop





