

@extends('master')


@section('navbar')

    @include('partials.navbar')
@stop




@section('content')
    <style type="text/css">
        .col_text_center{
            margin: auto;
            float: none;
        }
    </style>
    <div class="row">



        <div class="col-md-6 col_text_center">
            <div class="p-3 w-100">
                <!-- Header -->
                <div class="mb-3 text-center">

                    <p class="text-uppercase font-w700 font-size-sm text-muted">Update Work Report</p>
                </div>
                <form action="{{route('workreport.updateprocess',$workreport_info->id)}}" method="POST" role="form">
                    @method('put')
                    @csrf


                    <div class="form-group">
                        <label for="exampleInputTitle">Project Name</label>
                        <input name="project_name" type="name" value="{{$workreport_info->project_name}}"class="form-control" id="exampleInputTitle" placeholder="Enter Title" >
                    </div>


                    <div class="form-group" >
                        <label for="exampleInput"> Description </label>
                        <input name="description" value="{{$workreport_info->description}}" type="description" class="form-control" id="exampleInputDescription" aria-describedby="descriptionHelp" placeholder="Enter Description">
                    </div>


                    <button  type="submit" class="btn btn-primary form-control">Submit</button>
                </form>

            </div>

        </div>

    </div>
    </div>



@stop