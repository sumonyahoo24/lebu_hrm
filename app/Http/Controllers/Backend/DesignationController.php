<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Designation;

class DesignationController extends Controller
{
    public function list()
    {

        $all_list=Designation::all();
    	return view('emp_management.designation',compact('all_list'));
    }


    public function create(Request $request)
    {
        $this->validate($request,[
            'designation_name'=>'required'
        ]);
    	$designation_name=$request->input('designation_name');
       
    	Designation::create([

    			'designation_name'=>$designation_name               
    	]);
    	session()->flash('message', 'Designation Created Successfuly');

    	return redirect()->back();

    }
   public function update($id)
    {
        $designation_info=Designation::where('id',$id)->first();
        $designations=Designation::all();

        return view('emp_management.update_designation',compact('designation_info','designations'));
    }

    public function edit(Request $request,$id)
    {
        Designation::where('id',$id)->update([
                'designation_name'=>$request->input('designation_name')
        ]);
        $all_list=Designation::all();
        session()->flash('message', 'Designation Updated Successfuly');
        return view('emp_management.designation',compact('all_list'));
    }
}

