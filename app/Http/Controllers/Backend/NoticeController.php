<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use  Auth;

use App\Leave;
use App\Holidays;
use App\Notice;
use Carbon\Carbon;
use Session;


class NoticeController extends Controller
{
public function holidays()
{
 $holidays=Holidays::all();
 return view('Notices.holiday_setup', compact('holidays'));

}
public function holidays_create(Request $request)
{
     //dd($request->all());
  $today=date('Y-m-d');
  $dt1 =date($request->from_date);
  $dt2 = date($request->to_date);
  if(  $dt1<$dt2 and $dt1>$today )
  {
    Holidays::create([
      'holiday_name'=>$request->input('holiday_name'),  
      'from_date'=>$request->input('from_date'), 
      'to_date'=>$request->input('to_date'), 
      'holiday_description'=>$request->input('holiday_description'),    

    ]);

    Session::flash('message', 'Holiday Created Successfully'); 
       
      return redirect()->back();

  }else
  {
    Session::flash('message', 'From date should not be greater than To date!!'); 
       
      return redirect()->back();
  }

}
public function notices()
{
 $notices=Notice::all();
 return view('Notices.other_notices', compact('notices'));

}
public function notice_create(Request $request)
{
  
   Notice::create([
      'notice_title'=>$request->input('notice_title'),  
      
      'notice_description'=>$request->input('notice_description'),    

    ]); 
       
      return redirect()->back();

  }
public function update($id)
{
	$notice_info=Notice::where('id',$id)->first();
	$other_notices=Notice::all();


return view('Notices.update_notice', compact('notice_info','other_notices'));


}
public function updateprocess(Request $request,$id)
{
	//dd($request->all());
  Notice::where('id',$id)->update([
                'notice_title'=>$request->input('notice_title'),
                'notice_description'=>$request->input('notice_description')
        ]);
        $notices=Notice::all();
        return view('Notices.other_notices', compact('notices'));

}


}