<?php

namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Employee;
use App\Attendance;
use Illuminate\Support\Facades\DB;	
use PDF;
use Session;



class AttendanceController extends Controller
{
	public function show()
	{
		$user_id=auth()->user()->id;
		$all_list=Employee::where('user_id',auth()->user()->id)->get();
		
		// 'employee_name','user_id','email','phone_num','department_id','designation_id'
		$employee = DB::table('employees as e')
		->select('e.employee_name','dp.dept_name','ds.designation_name')
		->leftjoin('designations as ds','ds.id','e.designation_id')
		->leftjoin('departments as dp','dp.id','e.department_id')
		->where('user_id',$user_id)->first();

		$in = Attendance::select('in')->where('user_id',$user_id)->where('attendance_date',date("Y-m-d"))->first();

		

		$attendance=Attendance::where('user_id',auth()->user()->id)->get();

		return view('attendance_management.attendance_list',compact('employee','attendance','in'));
	}

	public function in()
	{
		$user_id=auth()->user()->id;
		$in=Attendance::select('in')->where('user_id',$user_id)->where('attendance_date',date("Y-m-d"))->first();
		if(empty($in))
		{
			$data=[
				'user_id'=>$user_id,
				'in'=>date('H:i:s'),
				'out'=>'',
				'attendance_date'=>date('Y-m-d')
			];

			Attendance::create($data);
			return "IN Time Taken Successfully";
		}
		else
		{
			return "Your are already insertrd your IN Time";
		}
		
	}
	public function out()
	{
		$user_id=auth()->user()->id;
		$in=Attendance::select('in')->where('user_id',$user_id)->where('attendance_date',date("Y-m-d"))->first();
		// dd($in);

		if(!empty($in))
		{
			$data=[
				'out'=>date('H:i:s')
			];

			Attendance::where('user_id',$user_id)->where('attendance_date',date("Y-m-d"))->update($data);
			return "OUT Time Updated Successfully";
		} else
		{
			return "We do not found your IN time ! Please Insert Your IN time";
		}

		
	}
	public function report()
	{
		$employees=Employee::all();
		$attendance='';
		$search_emp='';
		return view('attendance_management.attendance_report',compact('employees','attendance','search_emp'));
	}

	public function showReport(Request $request)
	{
		$emp_user_id=$request->input('user_id');
		$from_date=$request->input('from_date');
		$to_date=$request->input('to_date');
		

		// dd($request->all());

		if($from_date<$to_date)
		{
	
			$search_emp=Employee::where('user_id',$emp_user_id)->first();
			$attendance=Attendance::where('user_id',$emp_user_id)->whereBetween('attendance_date', [$from_date, $to_date])->get();
			$employees=Employee::all();
			
			Session::put('emp_id', $search_emp->user_id);
			Session::put('from_date', $from_date);
			Session::put('to_date', $to_date);
		
// dd(session('emp_id'));

			return view('attendance_management.attendance_report',compact('employees','attendance','search_emp'));

		}
		else
		{
			Session::flash('message', 'From date should not be greater than to date!!'); 
			return redirect()->back();
		}


	}

	public function export_pdf()
	{
		if(session()->has('emp_id')) {
			$emp_id = session('emp_id');
		} else {
			$emp_id = auth()->user()->id;
		}

		$from_date = session('from_date');
		$to_date = session('to_date');


		$attendance = Attendance::where('user_id',$emp_id)->whereBetween('attendance_date', [$from_date, $to_date])->get();
		$employees=Employee::all();


		$pdf = PDF::loadView('attendance_management.attendance_pdf', ['attendance' => $attendance]);
		//$pdf->save(storage_path().'_filename.pdf');

		return $pdf->download('attendance_pdf.pdf');
	}




}