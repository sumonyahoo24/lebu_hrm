<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkReport extends Model
{
    protected $guarded = [];

   public function client()
    	{
    		return $this->hasOne(Clients::class,'id','project_name');
    	}

}

