<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Department;
use App\Designation;

class Employee extends Model
{
   protected $fillable=[
    	'employee_name','user_id','email','phone_num','department_id','designation_id','photo'];

    	public function department()
    	{
    		return $this->hasOne(Department::class,'id','department_id');
    	}

    	public function designation()
    	{
    		return $this->hasOne(Designation::class,'id','designation_id');
    	}
}

